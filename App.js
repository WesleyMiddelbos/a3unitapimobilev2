import React from 'react';
import AuthNavigator from './src/navigators/AuthNavigator';

export default class App extends React.Component {
    render() {
        return (
            <AuthNavigator/>
        );
    }
}