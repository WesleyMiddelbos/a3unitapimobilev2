import React from 'react';

export default class UserStore extends React.Component{

    static loginUser(email, password){
        return fetch("http://127.0.0.1:8000/api/login/unit/"+1,{
            method: "post",
            header: {
                'Accept': "application/json",
                'Content-Type': "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
            .then((response) => response.json())
            .then((responseData) => {
                if(responseData.hasOwnProperty('status')){
                    return false;
                }else{
                    return responseData;
                }
            })
            .catch(error => {
                console.log(error);
                return false;
            });
    }

}