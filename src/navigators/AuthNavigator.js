import { createStackNavigator, createAppContainer, createDrawerNavigator, createSwitchNavigator, DrawerItems } from 'react-navigation';
import React from 'react';
import { SafeAreaView, ScrollView, Text, StyleSheet, View } from 'react-native';
import SplashScreen from "./../screens/SplashScreen";
import LoginScreen from "./../screens/LoginScreen";
import HomeScreen from "./../screens/HomeScreen";
import ProfileScreen from "./../screens/ProfileScreen";

const AuthNavigator = createStackNavigator({
        Splash: {screen: SplashScreen},
        Login: {screen: LoginScreen}
    },
    {
        initialRoute: "Splash"
    });

const CustomDrawerComponent = (props) => (
    <SafeAreaView style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
        <View style={styles.headerContainer}>
            <Text style={styles.headerText}>A3UnitApi - 11thamb</Text>
            <Text>Welkom Wesley Middelbos</Text>
        </View>
        <ScrollView>
            <DrawerItems {...props}/>
        </ScrollView>
    </SafeAreaView>
);

const AppNavigator = createDrawerNavigator({
    Home: {screen: HomeScreen},
    Profile: {screen: ProfileScreen}
}, {
    initialRoute: "Home",
    contentComponent: CustomDrawerComponent
});

export default createAppContainer(createSwitchNavigator(
    {
        Auth: AuthNavigator,
        App: AppNavigator
    },
    {
        initialRoute: "Auth"
    }
));

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    headerText: {
        flex: 1,
        fontWeight: "300",
        fontSize: 25,
        color: "#363636"
    },
    headerContainer: {
        height: 55,
        backgroundColor: "white",
        alignItems: 'center',
        justifyContent: 'center'
    }
});
