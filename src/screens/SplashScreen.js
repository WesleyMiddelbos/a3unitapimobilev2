import React from 'react';
import {StyleSheet, SafeAreaView, Text, Button, View, TouchableOpacity} from 'react-native';

export default class SplashScreen extends React.Component {

    constructor(props){
        super(props);
    }

    static navigationOptions = {
        header: null
    };

    render() {
        return (
            <View style={styles.mainContainer}>
                <SafeAreaView style={styles.container}>
                    <View style={styles.headerContainer}>
                        <Text style={styles.headerText}>A3UNIT API Mobile</Text>
                    </View>
                    <View style={styles.bottomContainer}>
                        <TouchableOpacity style={styles.button} onPress={() => this.props.navigation.navigate('Login')}>
                            <Text style={styles.buttonText}>Login</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.buttonBlue}>
                            <Text style={styles.buttonText}>Register</Text>
                        </TouchableOpacity>
                    </View>
                </SafeAreaView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: "#fff",
        flex: 1
    },
    container: {
        flex: 1
    },
    headerContainer: {
        flex: 1,
        alignItems: 'center',
    },
    headerText: {
        flex: 1,
        fontWeight: "300",
        fontSize: 25,
        color: "#363636"
    },
    bottomContainer: {
        flex: 1,
        justifyContent: 'flex-end'
    },
    button: {
        backgroundColor: "grey",
        marginLeft: 40,
        marginRight: 40,
        marginTop: 10,
        paddingTop: 10,
        paddingBottom: 10,
        borderRadius:10,
        borderWidth: 1,
        borderColor: '#fff'
    },
    buttonBlue: {
        backgroundColor: "#2C7DB2",
        marginLeft: 40,
        marginRight: 40,
        marginTop: 10,
        paddingTop: 10,
        paddingBottom: 10,
        borderRadius:10,
        borderWidth: 1,
        borderColor: '#fff'
    },
    buttonText: {
        color:'#fff',
        textAlign:'center',
        paddingLeft : 10,
        paddingRight : 10
    }
});