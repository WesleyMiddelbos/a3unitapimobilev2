import React from 'react';
import UserStore from './../../src/stores/UserStore';
import {View, Text, SafeAreaView, StyleSheet, TouchableOpacity, TextInput, ScrollView, Image} from 'react-native';
import HomeScreen from "./HomeScreen";
import {createDrawerNavigator} from "react-navigation";

export default class LoginScreen extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            email: null,
            password: null,
            selectedUnit: null,
        }
    }

    static navigationOptions = {
        title: "Login"
    };

    _loginUser(){
        if(this.state.email && this.state.password){
            // console.log(this.state.email, this.state.password);
            // let user = UserStore.loginUser(this.state.email, this.state.password);
            // console.log("user");
            // console.log(user);
            // if(user){
            //     alert(user.firstname);
            // }else{
            //     alert("User is not logged in");
            // }
            return this.props.navigation.navigate('App');
        }else{
            return this.props.navigation.navigate('App')
        }
    }

    _selecteUnit(){
        alert('unit selected');
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.formContainer}>
                    <View>
                        <Text style={styles.inputLabel}>Email</Text>
                        <TextInput allowFontScaling={true} keyboardType={"email-address"} style={styles.input} placeholder="Email" multiline={false} onChangeText={(value) => this.setState({email: value})}/>
                        <Text style={styles.inputLabel}>Password</Text>
                        <TextInput allowFontScaling={true} keyboardType={"default"} style={styles.input} placeholder="Password" secureTextEntry={true} multiline={false} onChangeText={(value) => this.setState({password: value})}/>
                        <ScrollView horizontal={true} scrollEventTrottle={16}>
                            <TouchableOpacity style={styles.scrollViewItem} onPress={() => this.setState({selectedUnit: 1})}>
                                <Image style={styles.scrollViewItemImage} source={require('./../../assets/img/logo.png')}/>
                                <Text style={styles.scrollViewItemText}>11th amb</Text>
                            </TouchableOpacity>
                        </ScrollView>
                    </View>
                    <TouchableOpacity style={styles.buttonBlue} onPress={() => this._loginUser()}>
                        <Text style={styles.buttonText}>Login</Text>
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    headerContainer: {
        flex: 1,
        alignItems: 'center'
    },
    headerText: {
        fontSize: 24,
        fontWeight: "700"
    },
    formContainer: {
        flex: 1,
        paddingHorizontal: 40,
        justifyContent: "center",
        marginBottom: 120
    },
    inputLabel: {
        fontSize: 18,
        fontWeight: "500"
    },
    input: {
        fontSize: 18,
        borderBottomColor: 'grey',
        borderBottomWidth: 1,
        paddingVertical: 10,
        marginBottom: 10
    },
    buttonBlue: {
        backgroundColor: "#2C7DB2",
        marginLeft: 40,
        marginRight: 40,
        marginTop: 10,
        paddingTop: 10,
        paddingBottom: 10,
        borderRadius:10,
        borderWidth: 1,
        borderColor: '#fff'
    },
    buttonText: {
        color:'#fff',
        textAlign:'center',
        paddingLeft : 10,
        paddingRight : 10
    },
    scrollViewItem: {
        flex:1,
        alignItems: 'center'
    },
    scrollViewItemImage: {
        width: 200,
        height: 200
    },
    scrollViewItemText: {
        fontSize: 20,
        fontWeight: "500"
    }
});