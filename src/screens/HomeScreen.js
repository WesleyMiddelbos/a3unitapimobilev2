import React from 'react';
import {StyleSheet, SafeAreaView, Text, View, TouchableOpacity} from 'react-native';
export default class HomeScreen extends React.Component {

    constructor(props){
        super(props);
    }

    static navigationOptions = {
        title: 'Welcome',
        drawerLabel: 'Home',
    };

    render() {
        const {navigate} = this.props.navigation;
        return (
            <SafeAreaView styles={styles.container}>
                <View style={styles.innerContainer}>
                    <Text style={styles.headerText}>Welkom in de omgeving van het 11thamb.</Text>
                    <Text>Uw gespeelde tijd is:</Text>
                    <Text>5 uur</Text>
                </View>
                <View style={styles.innerContainer}>
                    <TouchableOpacity style={styles.buttonBlue}>
                        <Text style={styles.buttonText}>Ga spelen</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.buttonBlue}>
                        <Text style={styles.buttonText}>Ga naar profiel</Text>
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
   container: {
       flex: 1,
       height: null
   },
    headerText: {
        fontWeight: "300",
        fontSize: 25,
        color: "#363636"
    },
    innerContainer: {
       padding: 20,
    },
    buttonBlue: {
        backgroundColor: "#2C7DB2",
        marginLeft: 40,
        marginRight: 40,
        marginTop: 10,
        paddingTop: 10,
        paddingBottom: 10,
        borderRadius:10,
        borderWidth: 1,
        borderColor: '#fff'
    },
    buttonText: {
        color:'#fff',
        textAlign:'center',
        paddingLeft : 10,
        paddingRight : 10
    }
});

